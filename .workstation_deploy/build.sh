#!/bin/bash -l

set -ex

## compile the java code using build.xml
ant compile -Dmpijar="$MPIJAR" -Djcompiler="$JCOMPILER"

cd build
cmake ../jni/
make
cp *bbtoolsjni.* ../resources/
cd -

## create the distribution using build.xml
ant dist -Dmpijar="$MPIJAR" -Djcompiler="$JCOMPILER"

## modify all shell scripts to use proper path to jar file
mkdir -p dist/bin
for i in `ls *sh`; do
    cat $i | sed "s|^CP=.*$|CP=\$DIR/../lib/BBTools.jar|g" > dist/bin/$i
    chmod a+rx dist/bin/$i
done
