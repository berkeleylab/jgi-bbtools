#!/bin/bash -l

set -ex

module purge
module load PrgEnv-gnu
module load openmpi/1.8.1
module load oracle-jdk/1.8_64bit
module load ant
module load ecj/4.6.0

export _JAVA_OPTIONS="-Xmx512M"

## compile the java code using build.xml
ant compile -lib $ECJ_DIR

cd build
cmake ../jni/
make
cp *bbtoolsjni.* ../resources/
cd -

## create the distribution using build.xml
ant dist -lib $ECJ_DIR

## modify all shell scripts to use proper path to jar file
mkdir -p dist/bin
for i in `ls *sh`; do
    cat $i | sed "s|^CP=.*$|CP=\$DIR/../lib/BBTools.jar|g" > dist/bin/$i
    chmod a+rx dist/bin/$i
done
