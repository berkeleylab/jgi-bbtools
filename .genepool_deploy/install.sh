#!/bin/bash -l

set -ex

if [ ! -e dist ]; then
    echo "dist doesn't exist; not build yet!"
    exit 1
fi

## modify all shell scripts to use proper path to jar file
mkdir -p dist/bin
mkdir -p dist/bin/docs
#mkdir -p dist/resources/
for i in `ls *sh`; do
    cat $i | sed "s|^DIR=.*$|DIR=$PREFIX/bin/|g" | sed "s|^CP=.*$|CP=$PREFIX/lib/BBTools.jar|g" | sed "s|^NATIVELIBDIR=.*$|NATIVELIBDIR=$PREFIX/lib|g" > dist/bin/$i
	chmod a+rx dist/bin/$i
done

for i in `ls docs/*.txt`; do
    cat $i > dist/bin/$i
	chmod a+r dist/bin/$i
done

cp resources/primes.txt.gz dist/lib/primes.txt.gz
chmod a+r dist/lib/primes.txt.gz

cp resources/*bbtoolsjni.* dist/lib/
chmod a+r dist/lib/*bbtoolsjni.*

rsync -a --delete dist/ $PREFIX/
